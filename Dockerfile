FROM debian:stretch

# Install basic dependencies. The directive is intentionally split into two parts, so you can probably reuse
# the images created by building cc-runner to save some disk space, since everything except the jdk is identical.
RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y \
    nginx build-essential python3-dev python3-pip python3-setuptools multitail vim
RUN apt-get install -y openjdk-8-jdk

# All application data will be available under /var/www/
COPY "." "/var/www/cc-runner-java"

WORKDIR /var/www/cc-runner-java

# Install python dependencies site-wide, since this is an isolated app in a container. We do not make use of a venv.
RUN pip3 install -r /var/www/cc-runner-java/requirements.txt

# We store a copy of the shipped java libraries under /opt to be compliant with FHS
# The 'JavaSDL'-class is expecting them there.
COPY "assets/java-libs" "/opt/java-libs"

# Set up uwsgi and nginx:
RUN mkdir -p /var/log/uwsgi/
RUN ln -s /var/www/cc-runner-java/assets/configs/nginx.conf /etc/nginx/conf.d/
RUN rm /etc/nginx/sites-enabled/default
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Expose nginx default port 80
EXPOSE 80

CMD uwsgi --ini /var/www/cc-runner-java/assets/configs/uwsgi.ini --daemonize /var/log/uwsgi/uwsgi.log && nginx
