import os
import time
from enum import Enum


class JavaSDL:
    """JavaSDL represents the Java Standard Directory Layout by Maven. It stores Java-Files on the filesystem.

    The member-variable `_global_classpath` contains the directory, in which some additional jar's are shipped."""

    class SourceType(Enum):
        """A Java-File must be of type `TEST` or type `SOURCE`."""
        TEST = 1
        SOURCE = 2

    _logger = None
    _base_dir = None
    _sources_dir = None
    _tests_dir = None
    _output_dir = None
    _global_classpath = '/opt/java-libs/'  # contains all shipped jar's

    def __init__(self, logger=None):
        """onstructs a new JavaSDL-object, which creates the Standard Directory Layout (by Maven) at `/tmp`.

        Under `tmp`, a session folder is created, e.g. /tmp/flask_pid-<pid>_<random_number>. All Java-Files will be
        written to it. The following source has more information about the structure inside the session folder:
        * https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html

        :param logger: Assign a flask.py-logger-object for logging, or `None` to disable logging.
        """
        self._logger = logger
        self._base_dir = '/tmp/' + 'flask_pid-' + str(os.getpid()) + '_' + str(round(time.time() * 1000)) + '/'

        self._sources_dir = self._base_dir + 'src/main/java/'
        self._tests_dir = self._base_dir + 'src/test/java/'
        self._output_dir = self._base_dir + 'out/'

        self._make_dir(self._sources_dir)
        self._make_dir(self._tests_dir)
        self._make_dir(self._output_dir)

    def get_base_dir(self):
        """Returns the base directory as String, which is the session-folder under `/tmp/flask_pid-<pid>_<rand-nr>`."""
        return self._base_dir

    def get_src_dir(self):
        """Returns the source-directory, which corresponds to `/tmp/flask_pid-pid_<rand-nr>/src/main/java`."""
        return self._sources_dir

    def get_test_dir(self):
        """Returns the test-directory, which corresponds to `/tmp/flask_pid-pid_<rand-nr>/src/test/java`."""
        return self._tests_dir

    def fqcn_to_path(self, fqcn):
        """Translates a java-fqcn to a directory-layout, returned as string.

        :param fqcn: Fully Qualified Class Name as string.
        :return: The directory-layout, separated by slashes ('/') as string.
        """
        if fqcn.count('.') == 0:  # fqcn is not part of a package
            return ""
        else:
            package_path = fqcn.split('.')[:-1]
            return '/'.join(package_path) + '/'

    def get_java_filename(self, fqcn):
        """Translates a Java-fqcn (fully qualified class name) to a file name, returned as string.

        :param fqcn: Fully Qualified Class Name as string.
        :return: The filename for the java-classfile as string.
        """
        # The number of dots in a fqcn represents the amount of packages, a class is part of.
        if fqcn.count('.') == 0:
            return fqcn + '.java'
        else:
            return fqcn.split('.')[-1] + '.java'

    def get_classpath(self, delimiter=':'):
        """Constructs a classpath, which contains the whole java project plus the global classpath.

        :param delimiter: Defaults to ':', which is the standard for linux. Delimits the class path.
        :return: The classpath as string, useful for the '-cp'-java-arg.
        """
        # add all subdirectories in _base_dir/src recursively
        classpath = [x[0] for x in os.walk(self._base_dir + 'src/')]

        for jar in os.listdir(self._global_classpath):
            if jar.endswith('.jar'):
                    classpath += [self._global_classpath + jar]

        ret = delimiter.join(classpath)
        return ret

    def write_javafile(self, fqcn, content, sourcetype):
        """Writes a java file to the java standard directory layout.

        :param fqcn: The fully qualified class name as string of the java-class, which shall be written.
        :param content: The content of the file as string.
        :param sourcetype: A `SourceType`-object, which denotes the type of the java-class (TEST or SOURCE).
        """
        assert (sourcetype == self.SourceType.SOURCE or sourcetype == self.SourceType.TEST)
        mode = 'x'

        if fqcn.endswith('.java'):
            raise NameError("FullyQualifiedClassName (FQCN) '" + fqcn + "' must not end with '.java'!")

        if self._logger is not None and (len(fqcn) == 0 or len(content) == 0):
            self._logger.warn("write_javafile(): ignoring file since content or fqcn is empty for fqcn: " + fqcn)

        if sourcetype == self.SourceType.SOURCE:
            path = self.get_src_dir()
        elif sourcetype == self.SourceType.TEST:
            path = self.get_test_dir()

        # check if file is part of standardpackage and fqcn consists of more than only '.java' (len = 5 characters)
        if fqcn.count('.') == 0:
            # if given fqcn is a java-file in standard-package
            with open(path + fqcn + '.java', mode) as file:
                file.write(content)

                if self._logger is not None:
                    self._logger.info("Wrote std-pkg file " + fqcn + " to " + path)
        else:
            # file is part of a package, e.g. "myPkg.HelloWorld"
            # first we create the package-path, i.e. the corresponding directories
            filename = self.get_java_filename(fqcn)

            # Make full path from extracted parts:
            path += self.fqcn_to_path(fqcn)

            # Create full path:
            self._make_dir(path)

            # Write file:
            with open(path + filename, mode) as file:
                file.write(content)
                if self._logger is not None:
                    self._logger.info("Wrote nonstd-pkg file " + fqcn + " to " + path + fqcn)

    def _make_dir(self, path):
        """Private function to create a directory, returns the created path as string."""
        if self._logger is not None:
            self._logger.debug("_make_dir():   " + path)
        os.makedirs(path, exist_ok=True)
        return path

    def _get_path(self, file):
        """Private function to return the path of some file as string."""
        return os.path.dirname(file)











