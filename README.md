# cc-runner-java

cc-runner's task is to provide a [ReST-API](https://en.wikipedia.org/wiki/Representational_state_transfer), which receives a list of source- and test-files and executes it. It does return its result to cc-runner, by whom cc-runner-java is started, monitored and terminated.

## Technical documentation

Please see [cc-runner-doc](https://gitlab.com/jliebers/cc-runner-doc) for the comprehensive technical documentation. This README-file acts as a cheat-sheet to the essence from a standpoint of development or deployment.

## Quickstart

These instructions will get you a copy of the project up and running on the same machine, cc-runner is running on.

### Method 1: Build an image from sourcecode

Clone this directory with `$ git clone git@gitlab.com:jliebers/cc-runner-java.git` to a location of your choice.
The repository has multiple branches, for developing-purpose please switch to `develop`: `$ git checkout develop`. The branch `master` is reserved for releases, to be merged from `develop`.

Enter the cloned folder with `$ cd cc-runner-java`. Build the sourcecode with the Dockerfile, i.e. `$ docker build -t cc-runner-java .`. The tag you provide with `-t` is used by cc-runner, to locate and start the image - you must provide it when calling cc-runner's API.

Once the image has been build and tagged appropriately and stored on the host-machine, on which cc-runner is running, nothing else has to be done. There is no point in using `docker run` to execute cc-runner-java, as this is the task of cc-runner.

### Method 2: Fetch from registry

Fetch the latest image from Gitlab's registry:

```
$ docker login gitlab.com:6666
$ docker pull gitlab.com:6666/jliebers/cc-runner-java
```

### Running the tests

Run the tests via the following command, after you have built the image as described below:
```
$ docker run -v /var/run/docker.sock:/var/run/docker.sock -e CC_HMAC_SECRET='s3cr3t' -e CC_BASICAUTH_SECRET='s3cr3t' --net cc-runner-net cc-runner-java python3 -m unittest -f -v tests/api_v1.py
```

Each tests has a DocString and generates messages, describing what went wrong and why.

### Deployment

This project is deployed by Docker as described in the provided [Dockerfile](Dockerfile). In addition to the sourcecode in this repository, the dockerfile installs and configures nginx to serve cc-runner via uWSGI in production.

To be started by cc-runner, it is only necessary to place this image on the same host, and adress it in cc-runner's API by its given tag.

## Built With

* [Flask](http://flask.pocoo.org/) - Awesome microframework for webdevelopment with Python
* [Docker](http://www.docker.com) - Containerizing applications for security and easy deployment.

## Authors

* **Liebers, Jonathan** - *Initial work and realization in WT 2017/18* - https://github.com/jliebers

## License

Please see the [LICENSE](LICENSE) file for details.